# Openscn edge components registry

This is a simple registry component that holds information regarding the edge components that uses the system and their dependent components

## Description

The edge components registry functions as an API between the registry data and the rest of the system. It holds information regarding what are the edge components, their authentication information and the data producing components (such as sensors).

## Details

This registry assumes that a device which has connection to the internet and posts values to the OpenSCN system is an edge device, no matter how powerful this device is. In order for the OpenSCN to accept input from the outer world, the device that sends the data should be registered and obtained a UUID. This component is not publicly available, but instead is being consumed by other components of the system (e.g. web api or http endpoint)

The system will generate the UUID and authenticate the device based on that UUID. This is not a super secure way to handle the edge components authentication, but with combination with other constraint provides a basic level of security and trust between the system and the edge components.

Each of the edge components is able to have dependent components. These components are either data producers or actuators (although actuators are not supported by the time this document is being written). The data producers could be anything that creates data such as sensors.

## Authentication

There are two types of authentication. Incoming and Outgoing data transfer. The incoming data flow is considered the posting of IoT data and is assign to a certain data producer or rejected. The outgoing data flow is considered to be the communication with the actuators. These types have their own authentication strategies set.
